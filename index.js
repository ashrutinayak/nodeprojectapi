var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.set('view engine', 'ejs');
app.use('/upload', express.static('upload'));
app.use('/video', express.static('video'));
app.use('/assets', express.static('assets'));
const SupportingURLRoutes = require('./route/supportingurl');
const GalleryRoute = require('./route/gallery');
const VideoRoute = require('./route/video');
const AboutRoute = require('./route/about');
const CarcategoryRoute = require('./route/carcategory');
const PrequalificationRoute = require('./route/prequalification');
const UserRoute = require('./route/user');
const FeedbackRoute = require('./route/feedback');
const ContactRoute = require('./route/contact');
const FavRoute = require('./route/fav');
const SavedcarRoute = require('./route/savedcar');
const CarRoute = require('./route/car');
const ReferralRoute = require('./route/referral');
const AdminRoute = require('./route/admin');
app.use('/supportingurl', SupportingURLRoutes);
app.use('/gallery', GalleryRoute);
app.use('/video', VideoRoute);
app.use('/about', AboutRoute);
app.use('/carcategory', CarcategoryRoute);
app.use('/prequalification', PrequalificationRoute);
app.use('/user', UserRoute);
app.use('/feedback', FeedbackRoute);
app.use('/contact', ContactRoute);
app.use('/fav', FavRoute);
app.use('/savedcar', SavedcarRoute);
app.use('/car', CarRoute);
app.use('/referral', ReferralRoute);
app.use('/admin', AdminRoute);
// set port
app.listen(3000, function () {
    console.log('Node app is running on port 3000');
});

module.exports = app;