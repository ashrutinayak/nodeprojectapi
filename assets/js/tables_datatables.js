$(function () {
  const dt = $('#datatable-example').dataTable(
    {
      isEnable: true,
      processing: true,
      serverSide: true,
      responsive: true,
      "ajax": {
        url: "studentinquriy",
        type: 'get'
      },
      "columns": [
        { "data": 'id' },
        { "data": 'name' },
        { "data": 'created_at' },
        { "data": 'phone' },
        { "data": 'program' },
        // { "data": 'course' },
        // { "data": 'coursename' },
        // { "data": 'referer' },
        // { "data": 'email' },
      ],
      "columnDefs": [
        {
          targets: 2,
          render: (a, e, t, n) => {
            var date = new Date(t.created_at);
            var admissiondate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            return (admissiondate);
          },
        },
        // {
        //   targets: 0,
        //   render: (a, e, t, n) => {
        //     var iDisplayIndex = 0;
        //     var index = iDisplayIndex + 1;
        //     return (index);
        //   },
        // },
      ],
    });
});
