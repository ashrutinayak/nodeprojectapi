$(function () {
    const dt = $('.datatables-demo').dataTable(
        {
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "ajax": {
                url: "studentinquriy",
                type: 'get'
            },
            "columns": [
                { "data": 'id' },
                { "data": 'name' },
                { "data": 'email' },
                { "data": 'phone' },
                { "data": 'comments' },
            ],
            // "columnDefs": [
            //     {
            //         targets: 2,
            //         render: (a, e, t, n) => {
            //             var date = new Date(t.created_at);
            //             var admissiondate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            //             return (admissiondate);
            //         },
            //     },
            // ]
        });
});
