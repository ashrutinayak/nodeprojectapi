$(function () {
    const dt = $('#mous-table').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listmous",
                type: 'get'
            },
            "columns": [
                { "data": 'mous_id' },
                { "data": 'mous_description' },
                { "data": 'mous_file' },
                { "data": 'mous_status' },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 2,
                    render: (a, e, t, n) => {
                        return `<a href="${t.mous_file}" target="_blank">Link</a>`;
                    },
                },

                {
                    targets: 3,
                    render: (a, e, t, n) => {
                        let html = "";

                        if (t.mous_status == 1) {
                            html += `<a href="mousstatus/${t.mous_id}" class="btn btn-sm btn-success status-btn" data-mous_id="${t.mous_id}" title="Click to De-activate">Active</a>`;
                        } else {
                            html += `<a href="mousstatus/${t.mous_id}" class="btn btn-sm btn-danger status-btn" data-mous_id="${t.mous_id}" title="Click to activate">InActive</a>`;
                        }

                        return html;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="mousdelete/${t.mous_id}"><button class="btn btn-danger m-2" data-id="${t.mous_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt4 = $('#Grant-table').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listgrant",
                type: 'get'
            },
            "columns": [
                { "data": 'mous_id' },
                { "data": 'mous_description' },
                { "data": 'mous_file' },
                { "data": 'mous_status' },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 2,
                    render: (a, e, t, n) => {
                        return `<a href="${t.mous_file}" target="_blank">Link</a>`;
                    },
                },

                {
                    targets: 3,
                    render: (a, e, t, n) => {
                        let html = "";

                        if (t.mous_status == 1) {
                            html += `<a href="mousstatus/${t.mous_id}" class="btn btn-sm btn-success status-btn" data-mous_id="${t.mous_id}" title="Click to De-activate">Active</a>`;
                        } else {
                            html += `<a href="mousstatus/${t.mous_id}" class="btn btn-sm btn-danger status-btn" data-mous_id="${t.mous_id}" title="Click to activate">InActive</a>`;
                        }

                        return html;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="mousdelete/${t.mous_id}"><button class="btn btn-danger m-2" data-id="${t.mous_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt1 = $('#datatable-example').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listdirector",
                type: 'get'
            },
            "columns": [
                { "data": 'director_id' },
                { "data": 'director_name' },
                { "data": 'director_image' },
                { "data": 'director_position' },
                { "data": "director_description" },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 2,
                    render: (a, e, t, n) => {
                        return `<img src="${t.director_image}" height="100px" width="100px">`;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="directordelete/${t.director_id}"><button class="btn btn-danger m-2" data-id="${t.director_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt2 = $('#Govering-body').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listgovering",
                type: 'get'
            },
            "columns": [
                { "data": 'director_id' },
                { "data": 'director_name' },
                { "data": 'director_image' },
                { "data": 'director_position' },
                { "data": "director_description" },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 2,
                    render: (a, e, t, n) => {
                        return `<img src="${t.director_image}" height="100px" width="100px">`;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="directordelete/${t.director_id}"><button class="btn btn-danger m-2" data-id="${t.director_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt3 = $('#AcademicCouncil').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listacademic",
                type: 'get'
            },
            "columns": [
                { "data": 'academic_councils_id' },
                { "data": 'academic_councils_name' },
                { "data": 'academic_councils_image' },
                { "data": 'academic_councils_position' },
                { "data": "academic_councils_description" },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 2,
                    render: (a, e, t, n) => {
                        return `<img src="${t.academic_councils_image}" height="100px" width="100px">`;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="academicdelete/${t.academic_councils_id}"><button class="btn btn-danger m-2" data-id="${t.academic_councils_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt5 = $('#Award-example').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listaward",
                type: 'get'
            },
            "columns": [
                { "data": 'award_id' },
                { "data": 'award_image' },
                { "data": 'award_name' },
                { "data": "award_description" },
                { "data": "award_date" },
                { "data": 'student_achivement_status' },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 1,
                    render: (a, e, t, n) => {
                        return `<img src="${t.award_image}" style="height: 100px;
                        width: 100px;"/>`;
                    },
                },

                {
                    targets: 5,
                    render: (a, e, t, n) => {
                        let html = "";

                        if (t.award_status == 1) {
                            html += `<a href="awardstatus/${t.award_id}" class="btn btn-sm btn-success status-btn" data-award_id="${t.award_id}" title="Click to De-activate">Active</a>`;
                        } else {
                            html += `<a href="awardstatus/${t.award_id}" class="btn btn-sm btn-danger status-btn" data-award_id="${t.award_id}" title="Click to activate">InActive</a>`;
                        }

                        return html;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="awarddelete/${t.award_id}"><button class="btn btn-danger m-2" data-id="${t.award_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt6 = $('#Studnet-Achievement-example').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "liststudentachievement",
                type: 'get'
            },
            "columns": [
                { "data": 'student_achivement_id' },
                { "data": 'student_achivement_image' },
                { "data": 'student_achivement_name' },
                { "data": "student_achivement_description" },
                { "data": "student_achivement_date" },
                { "data": 'student_achivement_status' },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 1,
                    render: (a, e, t, n) => {
                        return `<img src="${t.student_achivement_image}" style="height: 100px;
                            width: 100px;"/>`;
                    },
                },

                {
                    targets: 5,
                    render: (a, e, t, n) => {
                        let html = "";

                        if (t.student_achivement_status == 1) {
                            html += `<a href="studentachievementstatus/${t.student_achivement_id}" class="btn btn-sm btn-success status-btn" data-student_achivement_id="${t.student_achivement_id}" title="Click to De-activate">Active</a>`;
                        } else {
                            html += `<a href="studentachievementstatus/${t.student_achivement_id}" class="btn btn-sm btn-danger status-btn" data-student_achivement_id="${t.student_achivement_id}" title="Click to activate">InActive</a>`;
                        }

                        return html;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="studentachievementdelete/${t.student_achivement_id}"><button class="btn btn-danger m-2" data-id="${t.student_achivement_id}">Delete</button></a>`;
                    }
                },
            ],
        });
    const dt7 = $('#Ranking-example').dataTable(
        {
            isEnable: true,
            processing: true,
            serverSide: true,
            responsive: true,
            "ajax": {
                url: "listranking",
                type: 'get'
            },
            "columns": [
                { "data": 'ranking_id' },
                { "data": 'ranking_image' },
                { "data": 'ranking_name' },
                { "data": "ranking_title" },
                { "data": "ranking_description" },
                { "data": 'ranking_status' },
                { "data": null },
                // { "data": 'course' },
                // { "data": 'coursename' },
                // { "data": 'referer' },
                // { "data": 'email' },
            ],
            "columnDefs": [
                {
                    targets: 1,
                    render: (a, e, t, n) => {
                        return `<img src="${t.ranking_image}" style="height: 100px;
                                width: 100px;"/>`;
                    },
                },

                {
                    targets: 5,
                    render: (a, e, t, n) => {
                        let html = "";

                        if (t.ranking_status == 1) {
                            html += `<a href="rankingstatus/${t.ranking_id}" class="btn btn-sm btn-success status-btn" data-ranking_id="${t.ranking_id}" title="Click to De-activate">Active</a>`;
                        } else {
                            html += `<a href="rankingstatus/${t.ranking_id}" class="btn btn-sm btn-danger status-btn" data-ranking_id="${t.ranking_id}" title="Click to activate">InActive</a>`;
                        }

                        return html;
                    },
                },
                {
                    targets: -1,
                    render: (a, e, t, n) => {
                        return `<a href="rankingdelete/${t.ranking_id}"><button class="btn btn-danger m-2" data-id="${t.ranking_id}">Delete</button></a>`;
                    }
                },
            ],
        });
});
