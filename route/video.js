var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
// Retrieve all Support Url
Router.get('/', function (req, res) {
    db.query('SELECT * FROM video', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            var result = [];
        results.forEach(element => {
            result.push({ 'video_id': element.video_id, 'video_image': config + element.video_image, 'video_url': config + element.video_file });
            console.log(result);
        });
        return res.send({ status: 1, data: result, msg: 'Video List.' });
    });
});

module.exports = Router;