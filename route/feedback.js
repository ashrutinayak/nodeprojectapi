var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
// Feedback Add.
Router.post('/', function (req, res) {
    let user = { title: req.body.title, message: req.body.message };
    db.query("INSERT INTO feedback SET ? ", user, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            return res.send({ status: 1, data: results, message: 'New Feedback has been created successfully.' });
    });
});
module.exports = Router;