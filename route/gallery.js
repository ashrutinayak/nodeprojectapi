var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
// Retrieve all Support Url
Router.get('/', function (req, res) {
    db.query('SELECT * FROM gallery', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            var result = [];
        results.forEach(element => {
            result.push({ 'gallery_id': element.gallery_id, 'gallery_image': config + element.gallery_image });
            console.log(result);
        });
        return res.send({ status: 1, data: result, msg: 'Gallery Images List.' });
    });
});

module.exports = Router;