var express = require('express');
const Router = express.Router();
var db = require('./../connection');
// Retrieve all Support Url
Router.get('/', function (req, res) {
    db.query('SELECT * FROM support_url', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            return res.send({ status: 1, data: results[0], msg: 'Supporting Url list.' });
    });
});

module.exports = Router;