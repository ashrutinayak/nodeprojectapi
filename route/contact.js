var express = require('express');
var md5 = require('md5');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
var transporter = require('./../emailconfig');
var randomstring = require("randomstring");

//Contact
Router.post('/', function (req, res) {
    let user = { contact_name: req.body.contact_name, contact_email: req.body.contact_email, contact_msg: req.body.contact_msg };
    db.query("INSERT INTO contact SET ? ", user, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            var email = req.body.contact_email;
        var mailOptions = {
            from: 'ashrutinayak2697@gmail.com',
            to: email,
            subject: 'Contact email node js.',
            text: 'Thank you for contact us ' + req.body.contact_name
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                return res.send({ status: 1, data: results, msg: 'Contact Successful.' });
            }
        });
    });
});
module.exports = Router;