var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
// Prequalification User Add.
Router.post('/signup', function (req, res) {
    let user = { user_id: req.body.user_id, firstName: req.body.firstName, lastName: req.body.lastName, email: req.body.email, phone: req.body.phone, ssn: req.body.ssn, dob: req.body.dob, homeAddress: req.body.homeAddress, aptNo: req.body.aptNo, city: req.body.city, state: req.body.state, zip: req.body.zip, years: req.body.years, months: req.body.months, wantOn: req.body.wantOn, monthlyPayment: req.body.monthlyPayment, employmentStatus: req.body.employmentStatus, employer: req.body.employer, jobTitle: req.body.jobTitle, jobYears: req.body.jobYears, jobMonths: req.body.jobMonths, grossIncome: req.body.grossIncome, device_id: req.body.device_id };
    db.query('SELECT * FROM prequalification where user_id=' + req.body.user_id, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else {
            console.log(results)
            if (results == "") {
                if (!user) {
                    return res.send({ status: 0, error: true, message: 'Please provide Prequalification Details.' });
                }
                db.query("INSERT INTO prequalification SET ? ", user, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        return res.send({ status: 1, data: results, message: 'New Prequalification has been created successfully.' });
                });
            }
            else {
                let pre_id = results[0].pre_id;
                console.log(pre_id);
                db.query('UPDATE prequalification SET ? WHERE pre_id = ?', [user, pre_id], function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        return res.send({ status: 1, data: results, message: 'Update Prequalification has been successfully.' });
                });
            }

        }

    });
});

// Prequalification user login
Router.post('/login', function (req, res) {
    db.query('SELECT * FROM prequalification where lastName ="' + req.body.lastName + '" and ssn =' + req.body.ssn + ' and zip =' + req.body.zip, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (results != "") {
                let pre_id = results[0].pre_id;
                let user = { device_id: req.body.device_id };
                db.query('UPDATE prequalification SET ? WHERE pre_id = ?', [user, pre_id], function (error, result, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        return res.send({ status: 1, data: results[0], msg: 'Login Successful.' });
                });
            }
    });
});

// Prequalification user add prequalification co borrow
Router.post('/signup', function (req, res) {
    let user = { user_id: req.body.user_id, firstName: req.body.firstName, lastName: req.body.lastName, email: req.body.email, phone: req.body.phone, ssn: req.body.ssn, dob: req.body.dob, homeAddress: req.body.homeAddress, aptNo: req.body.aptNo, city: req.body.city, state: req.body.state, zip: req.body.zip, years: req.body.years, months: req.body.months, wantOn: req.body.wantOn, monthlyPayment: req.body.monthlyPayment, employmentStatus: req.body.employmentStatus, employer: req.body.employer, jobTitle: req.body.jobTitle, jobYears: req.body.jobYears, jobMonths: req.body.jobMonths, grossIncome: req.body.grossIncome, co_firstName: req.body.co_firstName, co_lastName: req.body.co_lastName, co_email: req.body.co_email, co_phone: req.body.co_phone, co_ssn: req.body.co_ssn, co_dob: req.body.co_dob, co_homeAddress: req.body.co_homeAddress, co_aptNo: req.body.co_aptNo, co_city: req.body.co_city, co_state: req.body.co_state, co_zip: req.body.co_zip, co_years: req.body.co_years, co_months: req.body.co_months, co_wantOn: req.body.co_wantOn, co_monthlyPayment: req.body.co_monthlyPayment, co_employmentStatus: req.body.co_employmentStatus, co_employer: req.body.co_employer, co_jobTitle: req.body.co_jobTitle, co_jobMonths: req.body.co_jobMonths, co_grossIncome: req.body.co_grossIncome, co_jobYears: req.body.co_jobYears, device_id: req.body.device_id };
    db.query("INSERT INTO prequalificationco SET ? ", user, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            return res.send({ status: 1, data: results, message: 'New Prequalification Co Borrow has been created successfully.' });
    });
});

module.exports = Router;