var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
// Retrieve all Support Url
Router.get('/', function (req, res) {
    db.query('SELECT * FROM category_car where category_name != ""', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            var result = [];
        results.forEach(element => {
            result.push({ 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
            console.log(result);
        });
        return res.send({ status: 1, data: result, msg: 'Gallery Images List.' });
    });
});

module.exports = Router;