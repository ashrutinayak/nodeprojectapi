var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');

// fav
Router.post('/', function (req, res) {
    db.query('SELECT * FROM fav where user_id =' + req.body.user_id + ' and car_id =' + req.body.car_id, function (error, favresult, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (favresult != "") {
                if (favresult[0].status == 0) {
                    let user_id = favresult[0].fav_id;
                    let user = { status: 1 };
                    db.query('UPDATE fav SET ? WHERE fav_id = ?', [user, user_id], function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            console.log(user_id);
                            db.query('SELECT * FROM fav where status = 1' + ' and car_id =' + req.body.car_id, function (error, results, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    let user_id = favresult[0].fav_id;
                                    let total_like = results.length;
                                    let user = { total_like: total_like };
                                    db.query('UPDATE fav SET ? WHERE fav_id = ?', [user, user_id], function (error, result, fields) {
                                        if (error)
                                            return res.send({ status: 0, msg: error });
                                        else {
                                        }
                                    });
                                    db.query('SELECT * FROM fav where status = 1' + ' and car_id =' + req.body.car_id, function (error, results, fields) {
                                        if (error)
                                            return res.send({ status: 0, msg: error });
                                        else {
                                            var users = { car_id: req.body.car_id, user_id: req.body.user_id };
                                            db.query("INSERT INTO saved_car SET ? ", users, function (error, results, fields) {
                                                if (error)
                                                    return res.send({ status: 0, msg: error });
                                                else
                                                    return res.send({ status: 1, data: results, message: 'New Feedback has been created successfully.' });
                                            });
                                        }
                                    });

                                }
                            });
                        }
                    });
                }
                else if (favresult[0].status == 1) {
                    let user_id = favresult[0].fav_id;
                    let user = { status: 0 };
                    db.query('UPDATE fav SET ? WHERE fav_id = ?', [user, user_id], function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            db.query('SELECT * FROM fav where status = 1' + ' and car_id =' + req.body.car_id, function (error, results, fields) {
                                if (error)
                                    return res.send({ status: 0, msg: error });
                                else {
                                    let total_like = results.length;
                                    let user = { total_like: total_like };
                                    db.query('UPDATE fav SET ? WHERE fav_id = ?', [user, user_id], function (error, result, fields) {
                                        if (error)
                                            return res.send({ status: 0, msg: error });
                                        else {
                                        }
                                    });
                                    db.query('DELETE FROM saved_car WHERE user_id =' + req.body.user_id + ' and car_id=' + req.body.car_id, function (error, results, fields) {
                                        if (error)
                                            return res.send({ status: 0, msg: error });
                                        else
                                            return res.send({ status: 1, data: results, message: 'New Feedback has been created successfully.' });
                                    });

                                }
                            });
                        }
                    });
                }
            }
            else {
                let user = { car_id: req.body.car_id, user_id: req.body.user_id };
                db.query("INSERT INTO fav SET ? ", user, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else {
                        db.query('SELECT * FROM fav where status = 1' + ' and car_id =' + req.body.car_id, function (error, results, fields) {
                            if (error)
                                return res.send({ status: 0, msg: error });
                            else {
                                let user_id = results[0].fav_id;
                                let total_like = results.length;
                                let user = { status: 1, total_like: total_like };
                                db.query('UPDATE fav SET ? WHERE fav_id = ?', [user, user_id], function (error, result, fields) {
                                    if (error)
                                        return res.send({ status: 0, msg: error });
                                    else {
                                    }
                                });
                                var users = { car_id: req.body.car_id, user_id: req.body.user_id };
                                console.log(users);
                                db.query("INSERT INTO saved_car SET ? ", users, function (error, results, fields) {
                                    if (error)
                                        return res.send({ status: 0, msg: error });
                                    else
                                        return res.send({ status: 1, data: results, message: 'New Feedback has been created successfully.' });
                                });

                            }
                        });
                    }
                });
            }
    });
});

module.exports = Router;