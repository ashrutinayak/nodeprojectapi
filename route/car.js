var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');

// Car list
Router.post('/', function (req, res) {
    db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where car.category_id IN (' + req.body.category_id + ')', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else {
            var result = [];
            results.forEach(element => {
                var favlike = 0;
                if (req.body.user_id != "" || req.body.user_id != null) {
                    db.query('SELECT * FROM fav  where car_id =' + req.body.keyword + ' and user_id=' + req.body.user_id, function (error, favresult, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                            favlike = favresult[0].status;
                        }
                    });
                }
                result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|"), 'total_like': favlike });
            });
            return res.send({ status: 1, data: result, msg: 'Car List.' });
        }
    });
});
//car search
Router.post('/search', function (req, res) {
    var result = [];
    if (req.body.type === 1) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where car.car_name  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else {
                if (req.body.user_id != "" || req.body.user_id != null) {
                    var users = { keyword: req.body.keyword, user_id: req.body.user_id, search_type: req.body.type };
                    db.query("INSERT INTO saved_car_search SET ? ", users, function (error, resultinsert, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else {
                        }
                    });
                }
                results.forEach(element => {
                    var favlike = 0;
                    if (req.body.user_id != "" || req.body.user_id != null) {
                        db.query('SELECT * FROM fav  where car_id =' + req.body.keyword + ' and user_id=' + req.body.user_id, function (error, favresult, fields) {
                            if (error)
                                return res.send({ status: 0, msg: error });
                            else {
                                favlike = favresult[0].status;
                            }
                        });
                    }
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|"), 'total_like': favlike });
                });
                return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
            }
        });
    }
    if (req.body.type === 2) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where car.car_model  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 4) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.exterior_color  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 5) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.interior_color  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 6) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.milage  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 7) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.engine_specification  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 8) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.vin_number  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 9) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.stock_count  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 10) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.transmission_type  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 11) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.seating  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }
    if (req.body.type === 12) {
        db.query('SELECT * FROM car inner join basic_specification on basic_specification.car_id = car.car_id inner join car_silder on car_silder.car_id = car.car_id inner join category_car on category_car.category_id = car.category_id where basic_specification.drive_train  LIKE "%' + req.body.keyword + '%"', function (error, results, fields) {
            if (error)
                return res.send({ status: 0, msg: error });
            else
                results.forEach(element => {
                    result.push({ 'car_id': element.car_id, 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'category_name': element.category_name, 'category_image': element.category_image.split("|") });
                });
            return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
        });
    }

});
module.exports = Router;