var express = require('express');
var md5 = require('md5');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
var transporter = require('./../emailconfig');
var randomstring = require("randomstring");
const jwt = require('jsonwebtoken');

// user signup
Router.post('/signup', function (req, res) {
    db.query('SELECT * FROM user where user_email ="' + req.body.user_email + '"', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (results == "") {
                token = Math.floor(100000 + Math.random() * 900000);
                var email = req.body.user_email;
                let user = { user_name: req.body.user_name, user_email: req.body.user_email, user_password: md5(req.body.user_password), device_id: req.body.device_id, user_token: token };
                db.query("INSERT INTO user SET ? ", user, function (error, results, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        var mailOptions = {
                            from: 'ashrutinayak2697@gmail.com',
                            to: email,
                            subject: 'Verification Code for node js.',
                            text: 'Your verification code is:' + token
                        };
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            return res.send({ status: 1, data: results, msg: 'Registre Successful.' });
                        }
                    });
                });
            }
            else {
                return res.send({ status: 2, msg: 'This email id is already used.' });
            }
    });
});

// user login
Router.post('/login', function (req, res) {
    db.query('SELECT * FROM user where user_email ="' + req.body.user_email + '" and user_password ="' + md5(req.body.user_password) + '"', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (results != "") {
                let user_id = results[0].user_id;
                let user = { device_id: req.body.device_id };
                db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else {
                        result = results[0];
                        jwt.sign({ result }, 'secretkey', (err, token) => {
                            return res.send({ status: 1, data: result, token, msg: 'Login Successful.' });
                        });
                    }
                });
            }
            else {
                return res.send({ status: 2, msg: 'Something Was Wrong.' });
            }
    });
});

// Social Login
Router.post('/social-login', function (req, res) {
    db.query('SELECT * FROM user where user_email ="' + req.body.user_email + '" and type ="' + req.body.type + '"', function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (results != "") {
                let user_id = results[0].user_id;
                let user = { device_id: req.body.device_id };
                db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        return res.send({ status: 1, data: results[0], msg: 'Login Successful.' });
                });
            }
            else {
                return res.send({ status: 2, msg: 'Something Was Wrong.' });
            }
    });
});

//Email verification
Router.post('/emailverification', function (req, res) {
    db.query('SELECT user_id,user_name,user_email,user_token,status FROM user where user_id =' + req.body.user_id, function (error, results, fields) {
        console.log(results[0].user_token);
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (results != "") {
                if (results[0].user_token == req.body.user_token) {
                    let user_id = results[0].user_id;
                    let user = { status: 1 };
                    db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                        if (error)
                            return res.send({ status: 0, msg: error });
                        else
                            return res.send({ status: 1, data: results[0], msg: 'Email Verification Successful.' });
                    });
                }
                else {
                    return res.send({ status: 2, msg: 'Your verification code is not correct.' });
                }
            }
            else {
                return res.send({ status: 2, msg: 'Something Was Wrong.' });
            }
    });
});

//ForgetPassword
Router.post('/forgetpassword', function (req, res) {
    db.query('SELECT * FROM user where user_email ="' + req.body.user_email + '"and device_id ="' + req.body.device_id + '"', function (error, results, fields) {
        var password = randomstring.generate(7);
        if (error)
            return res.send({ status: 0, msg: error });
        else
            if (results != "") {
                let user_id = results[0].user_id;
                let email = results[0].user_email;
                let user = { user_password: md5(password) };
                db.query('UPDATE user SET ? WHERE user_id = ?', [user, user_id], function (error, result, fields) {
                    if (error)
                        return res.send({ status: 0, msg: error });
                    else
                        var mailOptions = {
                            from: 'ashrutinayak2697@gmail.com',
                            to: email,
                            subject: 'New Password for node js.',
                            text: 'Your New Password is:' + password
                        };
                    transporter.sendMail(mailOptions, function (error, info) {
                        return res.send({ status: 1, data: results[0], msg: 'New Password Send in you email address Successful.' });
                    });
                });
            }
            else {
                return res.send({ status: 2, msg: 'Something Was Wrong.' });
            }
    });
});
module.exports = Router;