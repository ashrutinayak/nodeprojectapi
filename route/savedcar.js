var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
// saved car list
Router.post('/', function (req, res) {
    db.query('SELECT * FROM saved_car inner join basic_specification on basic_specification.car_id = saved_car.car_id inner join car_silder on car_silder.car_id = saved_car.car_id inner join car on car.car_id = saved_car.car_id where user_id=' + req.body.user_id, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            var result = [];
        results.forEach(element => {
            result.push({ 'saved_id': element.saved_id, 'car_id': element.car_id, 'user_id': element.user_id, 'bs_id': element.bs_id, 'fuel_type': element.fuel_type, 'exterior_color': element.exterior_color, 'interior_color': element.interior_color, 'milage': element.milage, 'engine_specification': element.engine_specification, 'vin_number': element.vin_number, 'stock_count': element.stock_count, 'transmission_type': element.transmission_type, 'seating': element.seating, 'drive_train': element.drive_train, 'car_silder_id': element.id, 'image': element.image.split("|"), 'category_id': element.category_id, 'used_miles': element.used_miles, 'car_series': element.car_series, 'car_model': element.car_model, 'car_price': element.car_price, 'car_name': element.car_name, 'car_description': element.car_description, 'mpg_city': element.mpg_city, 'mpg_highway': element.mpg_highway });
            console.log(result);
        });
        return res.send({ status: 1, data: result, msg: 'Saved Car List.' });
    });
});

// saved car search keyword
Router.post('/search', function (req, res) {
    db.query('SELECT * FROM saved_car_search  where user_id=' + req.body.user_id, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            return res.send({ status: 1, data: results, msg: 'Keyword List.' });
    });
});

// remove search keyword
Router.post('/searchremove', function (req, res) {
    db.query('DELETE FROM saved_car_search WHERE saved_car_search_id =' + req.body.saved_car_search_id, function (error, results, fields) {
        if (error)
            return res.send({ status: 0, msg: error });
        else
            return res.send({ status: 1, data: results, message: 'Remove keyword successfully.' });
    });
}),

    module.exports = Router;