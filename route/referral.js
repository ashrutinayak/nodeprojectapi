var express = require('express');
const Router = express.Router();
var db = require('./../connection');
var config = require('./../config');
var varification = require('./../verify');
const jwt = require('jsonwebtoken');
// add referral
Router.post('/', varification, function (req, res) {
    var result = [];
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            req.body.referrals.forEach(element => {
                var user_id = req.body.user_id;
                var full_name = element.full_name;
                var email = element.email;
                var phone = element.phone;
                result.push([user_id, full_name, email, phone]);
            });
            console.log(result);
            var query = "INSERT INTO refferal (user_id, full_name, email, phone) VALUES ?";
            db.query(query, [result], function (error, results, fields) {
                if (error)
                    return res.send({ status: 0, msg: error });
                else
                    return res.send({ status: 1, message: 'New Referral has been created successfully.' });
            });
        }
    });
});
module.exports = Router;